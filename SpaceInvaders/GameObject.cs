﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    class GameObject
    {
        protected Texture2D texture;
        protected int textureStates;
        protected int currentTexture;
        protected Vector2 position;
        protected Color color;
        protected Vector2 size;

        protected GameObject()
        {
            texture = null;
            textureStates = 0;
            currentTexture = 0;
            position = new Vector2(0, 0);
            size = new Vector2(0, 0);
            color = Color.White;
        }

        protected void Initiate(Vector2 position)
        {
            this.position = position;
        }

        public void SetTexture(Texture2D texture, int textureStates)
        {
            this.texture = texture;
            this.textureStates = textureStates;
            size = new Vector2(texture.Bounds.Width / textureStates, texture.Bounds.Height);
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) 
        {
            spriteBatch.Draw(texture, position, new Rectangle((int)size.X * currentTexture, 0, (int)size.X, (int)size.Y), color);
        }
    }
}
