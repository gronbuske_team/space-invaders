using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Text;

namespace SpaceInvaders
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        KeyboardState oldState;
        private enum GameState
        {
            Menu,
            Game,
            End
        }
        private GameState currentGameState;

        private Ship ship;
        private Alien[,] aliens;
        private Obstacle[] obstacles;
        private Bullet[] playerBullets;
        private Bullet[] alienBullets;

        private int bullets;
        private int aBullets;
        private int lives;
        private int score;
        private int[] highscore;
        private int highscoreSpot;
        private int shootRate;
        private Random rand;

        private Texture2D bulletTexture;
        private Texture2D backgroundTexture;
        private SpriteFont menuFont;

        private float backgroundPan;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            int height = GraphicsDevice.Viewport.Height;
            int width = GraphicsDevice.Viewport.Width;

            rand = new Random();
            shootRate = 10000;

            ship = new Ship(new Vector2(width / 2, height * 0.9f));
            aliens = new Alien[11,5];
            for (int i = 0; i < 11;  i++)
                for (int j = 0; j < 5; j++)
                    aliens[i, j] = new Alien(new Vector2(i * width * 0.06f, j * height * 0.06f));
            obstacles = new Obstacle[3];
            for (int i = 0; i < 3; i++)
                obstacles[i] = new Obstacle(new Vector2((i + 1) * width / 4, height * 0.85f));

            bullets = 0;
            playerBullets = new Bullet[64];
            for (int i = 0; i < 64; i++)
                playerBullets[i] = new Bullet(new Vector2(-10, -10));
            aBullets = 0;
            alienBullets = new Bullet[256];
            for (int i = 0; i < 256; i++)
                alienBullets[i] = new Bullet(new Vector2(-10, -10));

            lives = 3;
            score = 0;
            highscore = new int[5];
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@"highscore.txt");
                for (int i = 0; i < 5; i++)
                    highscore[i] = int.Parse(lines[i]);
            }
            catch (Exception)
            {
                FileStream fs = File.Create("highscore.txt");
                Byte[] info = new UTF8Encoding(true).GetBytes("0\r\n0\r\n0\r\n0\r\n0");
                fs.Write(info, 0, info.Length);
                fs.Close();
                for (int i = 0; i < 5; i++)
                    highscore[i] = 0;
            }
            highscoreSpot = -1;

            currentGameState = GameState.Menu;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Texture2D shipTexture = this.Content.Load<Texture2D>("ship");
            Texture2D alienTexture = this.Content.Load<Texture2D>("alien");
            Texture2D obstacleTexture = this.Content.Load<Texture2D>("obstacle");
            bulletTexture = this.Content.Load<Texture2D>("bullet");
            backgroundTexture = this.Content.Load<Texture2D>("background");

            menuFont = this.Content.Load<SpriteFont>("menuFont");

            ship.SetTexture(shipTexture, 3);
            for (int i = 0; i < 11; i++)
                for (int j = 0; j < 5; j++)
                    aliens[i, j].SetTexture(alienTexture, 2);
            for (int i = 0; i < 3; i++)
            {
                obstacles[i].SetTexture(obstacleTexture, 5);
            }
            for (int i = 0; i < 64; i++)
                playerBullets[i].SetTexture(bulletTexture, 1);
            for (int i = 0; i < 256; i++)
                alienBullets[i].SetTexture(bulletTexture, 1);
        }

        protected override void UnloadContent()
        {}

        private void LoseGame()
        {
            highscoreSpot = -1;
            currentGameState = GameState.End;
            bool newHigh = false;
            for (int i = 0; i < 5; i++ )
            {
                if (score > highscore[i])
                {
                    int temp = score;
                    score = highscore[i];
                    highscore[i] = temp;
                    if (!newHigh)
                    {
                        newHigh = true;
                        highscoreSpot = i;
                    }
                }
            }
            if (File.Exists("highscore.txt"))
                File.Delete("highscore.txt");
            try
            {
                FileStream fs = File.Create("highscore.txt");
                string highscoreString = "";
                for (int i = 0; i < 5; i++)
                    highscoreString += highscore[i].ToString() + "\r\n";
                Byte[] info = new UTF8Encoding(true).GetBytes(highscoreString);
                fs.Write(info, 0, info.Length);
                fs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            lives = 3;
            score = 0;
            shootRate = 10000;
        }

        private void LoseLife()
        {
            ship.Kill();
            for (int i = 0; i < 3; i++ )
            {
                obstacles[i].Rebuild();
            }
            lives--;
            if (lives <= 0)
                LoseGame();
            else
                for (int i = 0; i < 11; i++)
                    for (int j = 0; j < 5; j++)
                        aliens[i, j].Reset();
        }

        private void GameLoop(GameTime gameTime, KeyboardState newState)
        {
            ship.Update(gameTime);
            if ((newState.IsKeyDown(Keys.Up) && oldState.IsKeyUp(Keys.Up))
                || (newState.IsKeyDown(Keys.W) && oldState.IsKeyUp(Keys.W))
                || (newState.IsKeyDown(Keys.Space) && oldState.IsKeyUp(Keys.Space)))
            {
                Bullet newBullet = new Bullet(ship.GetPosition() + new Vector2(24, 0));
                newBullet.SetTexture(bulletTexture, 1);
                newBullet.SetSpeed(-3f);
                playerBullets[bullets % 64] = newBullet;
                bullets++;
            }
            for (int i = 0; i < 64; i++)
                playerBullets[i].Update(gameTime);
            Rectangle shipRec = ship.GetRectangle();
            for (int i = 0; i < 256; i++)
            {
                alienBullets[i].Update(gameTime);
                Rectangle bulletRec = alienBullets[i].GetRectangle();
                if (shipRec.Contains(bulletRec))
                {
                    LoseLife();
                    alienBullets[i].Remove();
                }
            }

            bool allDead = true;
            for (int i = 0; i < 11; i++)
                for (int j = 0; j < 5; j++)
                {
                    if (!aliens[i, j].IsDead())
                    {
                        allDead = false;
                        int randomNr = rand.Next(shootRate);
                        if (randomNr == 0)
                        {
                            Bullet newBullet = new Bullet(aliens[i, j].GetPosition() + new Vector2(24, 0));
                            newBullet.SetTexture(bulletTexture, 1);
                            newBullet.SetSpeed(3f);
                            alienBullets[aBullets % 256] = newBullet;
                            aBullets++;
                        }
                        aliens[i, j].Update(gameTime);
                        for (int k = 0; k < 64; k++)
                        {
                            Rectangle bulletRec = playerBullets[k].GetRectangle();
                            Rectangle alienRec = aliens[i, j].GetRectangle();
                            if (alienRec.Contains(bulletRec))
                            {
                                aliens[i, j].Kill();
                                playerBullets[k].Remove();
                                score++;
                                break;
                            }
                        }
                        if (aliens[i, j].IsAtEarth())
                        {
                            LoseLife();
                            lives++;
                            break;
                        }
                    }
                }
            if (allDead)
            {

                for (int i = 0; i < 11; i++)
                    for (int j = 0; j < 5; j++)
                    {
                        aliens[i, j].Reset();
                        aliens[i, j].IncreaseSpeed(0.6f);
                    }
                shootRate = (int)(shootRate * 0.5f);
            }

            for (int i = 0; i < 3; i++)
            {
                if (!obstacles[i].IsDestroyed())
                {
                    Rectangle obstacleRec = obstacles[i].GetRectangle();
                    obstacles[i].Update(gameTime);
                    for (int k = 0; k < 64; k++)
                    {
                        Rectangle bulletRec = playerBullets[k].GetRectangle();
                        if (obstacleRec.Intersects(bulletRec))
                        {
                            obstacles[i].Damage();
                            playerBullets[k].Remove();
                        }
                    }
                    for (int k = 0; k < 256; k++)
                    {
                        Rectangle aBulletRec = alienBullets[k].GetRectangle();
                        if (obstacleRec.Intersects(aBulletRec))
                        {
                            obstacles[i].Damage();
                            alienBullets[k].Remove();
                        }
                    }
                }
            }
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState newState = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || newState.IsKeyDown(Keys.Escape))
                this.Exit();

            backgroundPan += gameTime.ElapsedGameTime.Milliseconds * 0.1f;

            switch (currentGameState)
            {
                case GameState.Menu:
                    if (newState.IsKeyDown(Keys.Enter) && oldState.IsKeyUp(Keys.Enter))
                        currentGameState = GameState.Game;
                    break;
                case GameState.Game:
                    GameLoop(gameTime, newState);
                    break;
                case GameState.End:
                    if (newState.IsKeyDown(Keys.Enter) && oldState.IsKeyUp(Keys.Enter))
                        currentGameState = GameState.Menu;
                    break;
            }
            oldState = newState;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
            spriteBatch.Draw(backgroundTexture, new Vector2(), new Rectangle((int)backgroundPan, 100, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            switch (currentGameState)
            {
                case GameState.Menu:
                    float spriteLength = 3 * menuFont.MeasureString("SPACE INVADERS").X;
                    spriteBatch.DrawString(menuFont, "SPACE INVADERS", new Vector2((GraphicsDevice.Viewport.Width - spriteLength) / 2, 100), Color.White, 0.0f, new Vector2(0, 0), 3.0f, SpriteEffects.None, 0);
                    spriteLength = menuFont.MeasureString("Press Enter to start the game").X;
                    spriteBatch.DrawString(menuFont, "Press Enter to start the game", new Vector2((GraphicsDevice.Viewport.Width - spriteLength) / 2, 250), Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
                    break;
                case GameState.Game:
                    spriteBatch.DrawString(menuFont, score.ToString() + " Aliens killed", new Vector2(), Color.White);
                    spriteBatch.DrawString(menuFont, lives.ToString() + " Ships left", new Vector2(0, 16), Color.White);
                    ship.Draw(gameTime, spriteBatch);
                    for (int i = 0; i < 64; i++)
                        playerBullets[i].Draw(gameTime, spriteBatch);
                    for (int i = 0; i < 256; i++)
                        alienBullets[i].Draw(gameTime, spriteBatch);
                    for (int i = 0; i < 3; i++)
                        if (!obstacles[i].IsDestroyed())
                            obstacles[i].Draw(gameTime, spriteBatch);
                    for (int i = 0; i < 11; i++)
                        for (int j = 0; j < 5; j++)
                            if (!aliens[i, j].IsDead())
                                aliens[i, j].Draw(gameTime, spriteBatch);
                    break;
                case GameState.End:
                    spriteLength = 3 * menuFont.MeasureString("GAME OVER").X;
                    spriteBatch.DrawString(menuFont, "GAME OVER", new Vector2((GraphicsDevice.Viewport.Width - spriteLength) / 2, 100), Color.White, 0.0f, new Vector2(0, 0), 3.0f, SpriteEffects.None, 0);
                    int yOffset = 0;
                    for (int i = 0; i < 5; i++ )
                    {
                        yOffset += 20;
                        spriteLength = menuFont.MeasureString(highscore[i].ToString()).X;
                        spriteBatch.DrawString(menuFont, highscore[i].ToString(), new Vector2((GraphicsDevice.Viewport.Width - spriteLength) / 2, 170 + yOffset), i == highscoreSpot ? Color.Yellow : Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
                    }
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
