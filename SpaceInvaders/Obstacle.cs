﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    class Obstacle : GameObject
    {
        private int health;
        private bool destroyed;

        public Obstacle(Vector2 position)
        {
            health = 10;
            destroyed = false;
            position.X -= 33;
            base.Initiate(position);
        }

        public bool IsDestroyed()
        {
            return destroyed;
        }

        public void Damage()
        {
            health--;
            if (health <= 0)
                destroyed = true;
            currentTexture = (10 - health) / 2;
        }

        public void Rebuild()
        {
            health = 10;
            currentTexture = 0;
            destroyed = false;
        }
    }
}
