﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class Bullet : GameObject
    {
        private float speed;

        public Bullet(Vector2 position)
        {
            speed = 0;
            base.Initiate(position);
        }

        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }

        public void Remove()
        {
            speed = 0;
            position.X = -100;
        }

        public void Update(GameTime gameTime)
        {
            position.Y += speed;
        }
    }
}
