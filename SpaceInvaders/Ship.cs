﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceInvaders
{
    class Ship : GameObject
    {
        float speed;
        int stateTimer;

        public Ship(Vector2 position)
        {
            stateTimer = 0;
            speed = 0;
            base.Initiate(position);
        }

        public void Kill()
        {
            stateTimer = 0;
            currentTexture = textureStates - 1;
        }

        public void Update(GameTime gameTime)
        {
            stateTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (stateTimer > 400)
            {
                currentTexture = (currentTexture + 1) % (textureStates - 1);
                stateTimer = 0;
            }
            KeyboardState newState = Keyboard.GetState();

            if (newState.IsKeyDown(Keys.Left) || newState.IsKeyDown(Keys.A))
                speed -= 0.01f * gameTime.ElapsedGameTime.Milliseconds;
            else if (newState.IsKeyDown(Keys.Right) || newState.IsKeyDown(Keys.D))
                speed += 0.01f * gameTime.ElapsedGameTime.Milliseconds;
            else if (speed > 0)
                speed -= 0.004f * gameTime.ElapsedGameTime.Milliseconds;
            else if (speed < 0)
                speed += 0.004f * gameTime.ElapsedGameTime.Milliseconds;
            if (speed < 0.01f && speed > -0.01f)
                speed = 0;

            if (speed > 10)
                speed = 10;
            else if (speed < -10)
                speed = -10;

            position.X += speed;
            if (position.X < 0)
            {
                position.X = 0;
                speed = 0;
            }
            else if (position.X > 752)
            {
                position.X = 752;
                speed = 0;
            }
        }
    }
}
