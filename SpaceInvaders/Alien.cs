﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    class Alien : GameObject
    {
        private float walkedInX;
        private float walkedInY;
        private bool walkingRight;
        private float speedMultiplyer;
        const float baseSpeed = 0.09f;
        private bool dead;
        private int stateTimer;

        public Alien(Vector2 position)
        {
            stateTimer = 0;
            speedMultiplyer = 1;
            InitiateAlien();
            base.Initiate(position);
        }

        private void InitiateAlien()
        {
            walkedInX = 0;
            walkedInY = 0;
            walkingRight = true;
            dead = false;
        }

        public void IncreaseSpeed(float speed)
        {
            this.speedMultiplyer += speed;
        }

        public bool IsAtEarth()
        {
            return position.Y > 400;
        }

        public bool IsDead()
        {
            return dead;
        }

        public void Reset()
        {
            position.X -= walkedInX;
            position.Y -= walkedInY;
            InitiateAlien();
        }

        public void Kill()
        {
            dead = true;
        }

        public void Update(GameTime gameTime)
        {
            stateTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (stateTimer > 400)
            {
                currentTexture = (currentTexture + 1) % textureStates;
                stateTimer = 0;
            }
            if (walkingRight)
            {
                position.X += gameTime.ElapsedGameTime.Milliseconds * baseSpeed * speedMultiplyer;
                walkedInX += gameTime.ElapsedGameTime.Milliseconds * baseSpeed * speedMultiplyer;
                if (walkedInX > 270)
                {
                    walkingRight = false;
                    position.Y += 24;
                    walkedInY += 24;
                }
            }
            else
            {
                position.X -= gameTime.ElapsedGameTime.Milliseconds * baseSpeed * speedMultiplyer;
                walkedInX -= gameTime.ElapsedGameTime.Milliseconds * baseSpeed * speedMultiplyer;
                if (walkedInX < 0)
                {
                    walkingRight = true;
                    position.Y += 24;
                    walkedInY += 24;
                }
            }
        }
    }
}
